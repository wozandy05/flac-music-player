let allMusic = [
  {
  name: "Baby I Need You",
  artist: "Joosiq",
  img: "baby_i_need_you_sped",
  src: "baby_i_need_you_joosiq"
  },
  {
  name: "Baby I Need You",
  artist: "sped up 8282&Joosiq",
  img: "baby_i_need_you_sped",
  src: "baby_i_need_you_sped"
  },
  {
    name: "星茶会",
    artist: "灰澈",
    img: "星茶会",
    src: "星茶会"
  },
  {
    name: "You(=|)",
    artist: "BOL4",
    img: "you_blo4",
    src: "you_blo4"
  },
  {
    name: "Tourist",
    artist: "Rj Pasin",
    img: "tourist_rjpasin",
    src: "tourist_rjpasin"
  },
  {
    name: "Numb Little Bug",
    artist: "Em Beihold",
    img: "numb_little_bug",
    src: "numb_little_bug"
  },
  {
    name: "Beautiful Things",
    artist: "Benson Boone",
    img: "beautiful_things",
    src: "beautiful_things"
  },
  {
    name: "I Need A Girl",
    artist: "TaeYang",
    img: "i_need_a_girl",
    src: "i_need_a_girl"
  },
  {
    name: "Magnetic",
    artist: "ILLIT",
    img: "magnetic",
    src: "magnetic"
  },
  {
    name: "Wake (Studio)",
    artist: "Hillsong Young & Free",
    img: "wake_studio",
    src: "wake_studio"
  },
  {
    name: "Grew Up At Midnight",
    artist: "The Maccabees",
    img: "grew_up_at_midnight",
    src: "grew_up_at_midnight"
  },
  {
    name: "我曾遇到一束光",
    artist: "叶斯淳",
    img: "我曾遇到一束光",
    src: "我曾遇到一束光"
  },
  {
    name: "Love Is Bigger Than Anything In Its Way",
    artist: "U2&Cheat Codes",
    img: "love_is_bigger",
    src: "love_is_bigger"
  },
  {
    name: "意外",
    artist: "薛之谦",
    img: "意外",
    src: "意外"
  },
  {
    name: "你不知道的事",
    artist: "王力宏",
    img: "你不知道的事",
    src: "你不知道的事"
  },
  {
    name: "Shots (Broiler Remix)",
    artist: "Imagine Dragons&Broiler",
    img: "shots_broiler_remix",
    src: "shots_broiler_remix"
  },
  {
    name: "Closer",
    artist: "The Chainsmokers&Halsey",
    img: "closer",
    src: "closer"
  },
  {
    name: "There's Nothing Holdin' Me Back",
    artist: "Shawn Mendes",
    img: "hold_me_back",
    src: "hold_me_back"
  },
]